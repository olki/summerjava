package kz.aitu.oop.examples.task2;

public class MyString {

    private int[] values; // 1 2 3 4 5

    public MyString(int[] values) {
        this.values = values;
    }

    // return the number of values that are stored
    public int length() {
        return values.length;
    }

    // return the value stored at position or -1 if position is not available
    public int valueAt(int position) {
        if(position < 0 || position >= length()) return -1;
        return values[position];
    }
}
