package kz.aitu;

import kz.aitu.entity.Student;

public class Main {


    public static void main(String[] args) {
        int a = 5;
        int b = 10;

        a += b;

        //System.out.println("Hello to my first program!");
        //System.out.println("a is " + a);

        Student st1 = new Student();
        st1.setName("Kairat");
        st1.setAge(18);
        st1.setGroupName("IT first");
        st1.setGpa(3.8);


        Student st2 = new Student();
        st2.setName("Madina");
        st2.setAge(17);
        st2.setGroupName("IT first");
        st2.setGpa(3.9);

        System.out.println(st1.toString());
        System.out.println(st2.toString());




        Student st3 = new Student();
        st3.setName("Assel");
        st3.setAge(20);
        st3.setGroupName("IT second");
        st3.setGpa(2.5);
        System.out.println(st3.toString());


        Student st4 = new Student("Aman", 19);
        st4.setGpa(3.5);
        st4.setGroupName("Third group");
        System.out.println(st4.toString());


        Student st5 = new Student("Aruzhan", 19, 4.0, "Fifth");
        System.out.println(st5);


        Student st6 = new Student("NewNAme", 19, 4.0, "Fifth");
        System.out.println(st6);





    }
}
