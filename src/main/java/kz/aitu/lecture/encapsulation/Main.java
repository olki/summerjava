package kz.aitu.lecture.encapsulation;

public class Main {


    public static void main(String[] args) {
        //BankAccount bankAccount = new BankAccount();
        //bankAccount.balance = 54.2;
        //bankAccount.creditNumber = "4208 2323 2414 1515";
        //System.out.println(bankAccount.creditNumber);
        //bankAccount.balance = 900;
        //System.out.println(bankAccount.balance);
        //bankAccount.creditNumber = "5555 2323 2414 1515";
        //bankAccount.balance = 999999999999.9;
        //System.out.println(bankAccount.creditNumber);
        //System.out.println(bankAccount.balance);

        BankAccount bankAccount = new BankAccount("4208 5555 4444 3333");
        bankAccount.addMoney(9999.9);
        System.out.println(bankAccount.getBalance());
        bankAccount.minusMoney(800);
        System.out.println(bankAccount.getBalance());
        System.out.println(bankAccount.getCreditNumber());
    }
}
