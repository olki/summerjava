package kz.aitu.lecture.encapsulation;

public class BankAccount {

    private String creditNumber;
    private double balance = 0.0;

    public BankAccount(String creditNumber) {
        this.creditNumber = creditNumber;
    }

    public void addMoney(double money) {
        if(money <= 0) return;
        balance += money;
    }

    public void minusMoney(double money) {
        if(money <= 0) return;
        balance -= money;
    }

    public double getBalance() {
        return balance;
    }

    public String getCreditNumber() {
        return creditNumber;
    }
}
