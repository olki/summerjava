package kz.aitu.lecture.polymorphism;

public class Main {


    public static void main(String[] args) {
        Animal animal1 = new Animal("animal one");
        Bird bird1 = new Bird("Bird first");
        Bird bird2 = new Bird("Bird second");
        Dog dog1 = new Dog("dogOne");
        Dog dog2 = new Dog("dogTwo");
        Dog dog3 = new Dog("dogThree");

        Animal animal00 = new Dog("dog");
        Animal animal01 = new Bird("bird");


        Animal[] animals = new Animal[8];
        animals[0] = animal1;
        animals[1] = bird1;
        animals[2] = bird2;
        animals[3] = dog1;
        animals[4] = dog2;
        animals[5] = dog3;
        animals[6] = animal00;
        animals[7] = animal01;

        int countDog = 0;
        for (int i = 0; i < animals.length; i++) {
            if(animals[i] instanceof Dog) {
                countDog++;
                Dog dog = (Dog)animals[i];
                dog.run();
            } else if(animals[i] instanceof Bird) {
                Bird bird = (Bird)animals[i];
                bird.fly();
            }
        }

        System.out.println("Dog number is " + countDog);



    }
}
