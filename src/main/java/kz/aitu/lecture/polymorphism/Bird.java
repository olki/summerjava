package kz.aitu.lecture.polymorphism;

public class Bird extends Animal{

    public Bird(String name) {
        super(name);
    }

    public void fly() {
        System.out.println("The bird is flying...");
    }

    public void fly(int height) {
        System.out.println("The " + getName() + " is flying " + height + "meter high.");
    }
}
