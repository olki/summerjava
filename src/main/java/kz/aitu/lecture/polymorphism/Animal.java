package kz.aitu.lecture.polymorphism;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void eat() {
        System.out.println("Animal eats....");
    }

    public String getName() {
        return name;
    }
}
