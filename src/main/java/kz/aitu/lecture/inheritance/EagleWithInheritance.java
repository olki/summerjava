package kz.aitu.lecture.inheritance;

public class EagleWithInheritance extends Bird {

    private String name;
    private int lifespan;

    public EagleWithInheritance(String reproduction, String outerCovering, String name, int lifespan) {
        super(reproduction, outerCovering);
        this.name = name;
        this.lifespan = lifespan;
    }

    public void flyUp(String par1) {
        System.out.println("Eagle flying....");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLifespan() {
        return lifespan;
    }

    public void setLifespan(int lifespan) {
        this.lifespan = lifespan;
    }
}
