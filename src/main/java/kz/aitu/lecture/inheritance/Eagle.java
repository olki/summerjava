package kz.aitu.lecture.inheritance;

public class Eagle {

    private String reproduction = "egg";
    private String outerCovering = "feather";
    private String name = "eagle";
    private int lifespan = 15;

    public Eagle() {
    }

    public Eagle(String reproduction, String outerCovering, String name, int lifespan) {
        this.reproduction = reproduction;
        this.outerCovering = outerCovering;
        this.name = name;
        this.lifespan = lifespan;
    }

    public void flyUp() {
        System.out.println("Flying up..");
    }

    public void flyDown() {
        System.out.println("Flying down..");
    }


    public String getReproduction() {
        return reproduction;
    }

    public void setReproduction(String reproduction) {
        this.reproduction = reproduction;
    }

    public String getOuterCovering() {
        return outerCovering;
    }

    public void setOuterCovering(String outerCovering) {
        this.outerCovering = outerCovering;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLifespan() {
        return lifespan;
    }

    public void setLifespan(int lifespan) {
        this.lifespan = lifespan;
    }
}
