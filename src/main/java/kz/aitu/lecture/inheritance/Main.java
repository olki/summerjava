package kz.aitu.lecture.inheritance;

public class Main {

    public static void main(String[] args) {
        System.out.println("Test our main class in inheritance package");

        EagleWithInheritance eagle = new EagleWithInheritance("egg2", "feather2", "eagle", 14);

        eagle.flyUp();
    }
}
