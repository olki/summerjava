package kz.aitu.lecture.inheritance;

public class Bird {

    private String reproduction = "egg";
    private String outerCovering = "feather";

    public Bird() {
    }

    public Bird(String reproduction, String outerCovering) {
        this.reproduction = reproduction;
        this.outerCovering = outerCovering;
    }

    public void flyUp() {
        System.out.println("Bird flying up..");
    }

    public void flyDown() {
        System.out.println("Flying down..");
    }

    public String getReproduction() {
        return reproduction;
    }

    public void setReproduction(String reproduction) {
        this.reproduction = reproduction;
    }

    public String getOuterCovering() {
        return outerCovering;
    }

    public void setOuterCovering(String outerCovering) {
        this.outerCovering = outerCovering;
    }
}
